﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Graphics
{
    public abstract class Completable : ICompletable
    {
        /// <summary>
        /// The current status of the event--either queued (hasn't started yet), in progress, or complete
        /// </summary>
        public CompletionStatus Status { get; protected set; }

        public Completable()
        {
            Status = CompletionStatus.QUEUED;
            Reset();
        }

        public virtual void Start()
        {
            Status = CompletionStatus.IN_PROGRESS;
            Reset();
        }

        public virtual void Terminate()
        {
            Status = CompletionStatus.COMPLETE;
        }

        public abstract void OnUpdate(GameTime time);
        public abstract void OnDraw(GameTime time);

        protected abstract void Reset();
    }
}
