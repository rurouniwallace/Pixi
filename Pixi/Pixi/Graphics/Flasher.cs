﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Graphics
{ 
    /// <summary>
    /// Utility class to help render an element that is meant to flash a fixed number of times
    /// </summary>
    public class Flasher : Completable
    {
        /// <summary>
        /// Position on the screen to draw the flashing element
        /// </summary>
        public Vector2 ScreenPosition { get; set; }

        /// <summary>
        /// Is the flashing element currently displayed?
        /// </summary>
        public bool IsDisplayed { get; private set; }

        /// <summary>
        /// Number of seconds to display the element before it disappears
        /// </summary>
        private double _period;

        /// <summary>
        /// Number of times to flash
        /// </summary>
        private int _numFlashes;

        /// <summary>
        /// Scale factor
        /// </summary>
        private Single _scale;

        /// <summary>
        /// Current running total number of flashes
        /// </summary>
        private int _currentNumFlashes;

        /// <summary>
        /// Sprite batch to use to draw the element
        /// </summary>
        private SpriteBatch _spriteBatch;

        /// <summary>
        /// Image to draw
        /// </summary>
        private Texture2D _renderTarget;

        /// <summary>
        /// Number of seconds since last transition between display statuses
        /// </summary>
        private double _secondsSinceLastTransition;

        /// <summary>
        /// Image width
        /// </summary>
        public float Width
        {
            get
            {
                return _renderTarget.Width * _scale;
            }
        }

        /// <summary>
        /// Image height
        /// </summary>
        public float Height
        {
            get
            {
                return _renderTarget.Height * _scale;
            }
        }

        /// <summary>
        /// Construct a new instance
        /// </summary>
        /// <param name="period">number of seconds to display the image</param>
        /// <param name="numFlashes">Number of flashes</param>
        /// <param name="spriteBatch">Sprite batch to use to draw the image</param>
        /// <param name="renderTarget">The image to draw</param>
        public Flasher(double period, int numFlashes, SpriteBatch spriteBatch, Texture2D renderTarget) : base()
        {
            _period = period;
            _numFlashes = numFlashes;

            _currentNumFlashes = 0;
            IsDisplayed = true;
            _secondsSinceLastTransition = 0.0f;

            _spriteBatch = spriteBatch;
            _renderTarget = renderTarget;

            _scale = 0.2f;

            ScreenPosition = new Vector2(0.0f, 0.0f);
        }

        /// <summary>
        /// Draw the image, or don't, depending on display status
        /// </summary>
        /// <param name="time">Game timing state</param>
        override
        public void OnDraw(GameTime time)
        {
            if (IsDisplayed)
            {
                _spriteBatch.Draw(_renderTarget, ScreenPosition, null, Color.White, 0, new Vector2(0.0f, 0.0f), _scale, SpriteEffects.None, 0);
            }
        }

        /// <summary>
        /// Update the flasher
        /// </summary>
        /// <param name="gameTime">Game timing state</param>
        override
        public void OnUpdate(GameTime gameTime)
        {
            _secondsSinceLastTransition += gameTime.ElapsedGameTime.TotalSeconds;
            if (_currentNumFlashes >= _numFlashes)
            {
                Status = CompletionStatus.COMPLETE;
                IsDisplayed = false;
                return;
            }


            if (_secondsSinceLastTransition >= _period)
            {
                if (IsDisplayed)
                {
                    _currentNumFlashes += 1;
                }

                IsDisplayed = !IsDisplayed;
                _secondsSinceLastTransition = 0.0f;
            }
        }

        /// <summary>
        /// Reset state
        /// </summary>
        override
        protected void Reset()
        {
            _currentNumFlashes = 0;
            IsDisplayed = true;
            _secondsSinceLastTransition = 0.0f;
        }
    }
}
