﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Graphics
{
    /// <summary>
    /// The status of a completable element
    /// </summary>
    public enum CompletionStatus
    {
        QUEUED, IN_PROGRESS, COMPLETE
    }
}
