﻿using Microsoft.Xna.Framework;

namespace Pixi.Graphics
{
    public interface ICompletableDispatcher
    {
        void OnDraw(GameTime time);
        void OnUpdate(GameTime time);
        void registerCompletable(ICompletable completable);
    }
}