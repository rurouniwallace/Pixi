﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Pixi.Graphics;
using Pixi.Input;
using Pixi.Scenes;
using Pixi.Util;
using System;

namespace Pixi
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private IListensForInput _inputListener;
        private ISceneQueue _sceneQueue;
        private RenderTarget2D _renderTarget;
        private ICompletableDispatcher _completableDispatcher;
        private IExitsGame _gameExiter;

        private SpriteFont _font;

        public Game()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;

            // TODO use dependency injection
            _inputListener = new InputListener();
            _sceneQueue = new SceneQueue();
            _gameExiter = new GameExiter(this);
            _completableDispatcher = new CompletableDispatcher();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _font = Content.Load<SpriteFont>("Fonts/TastefulSandwich");

            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _renderTarget = new RenderTarget2D(GraphicsDevice, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height);

            

            SceneTitle titleScene = new SceneTitle(_sceneQueue, _inputListener, Content, _spriteBatch, _gameExiter, _completableDispatcher);
            _sceneQueue.AddSceneToQueue(titleScene);
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            _sceneQueue.UpdateCurrentScene(gameTime);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            _sceneQueue.DrawCurrentScene(gameTime);

            /*_spriteBatch.Begin();
            _spriteBatch.Draw((Texture2D)_renderTarget, Vector2.Zero, Color.White);

            _spriteBatch.End();*/

            base.Draw(gameTime);
        }
    }
}
