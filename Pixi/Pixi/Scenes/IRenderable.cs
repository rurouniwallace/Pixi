﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Scenes
{
    /// <summary>
    /// Element that can be rendered to the displays
    /// </summary>
    public interface IRenderable
    {
        #region Methods

        /// <summary>
        /// Actions to take upon startup
        /// </summary>
        public void OnStart();
        
        /// <summary>
        /// Actions to take when the renderable element stops
        /// </summary>
        public void OnStop();

        /// <summary>
        /// Actions to take on each frame update
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnUpdate(GameTime time);

        /// <summary>
        /// Draw the renderable
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void OnDraw(GameTime time);

        #endregion
    }
}
