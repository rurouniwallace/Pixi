﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Pixi.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Scenes
{
    /// <summary>
    /// Abstraction for a scene
    /// </summary>
    public abstract class Scene : Completable
    {
        // empty for now
        /// <summary>
        /// Build game pad bindings. Since only player one will ever have bindings, this saves us some
        /// boilerplate
        /// </summary>
        /// <typeparam name="T">the type of command to bind</typeparam>
        /// <param name="command">The command to bind</param>
        /// <returns></returns>
        protected Dictionary<PlayerIndex, T> BuildGamePadBinding<T>(T command)
        {
            Dictionary<PlayerIndex, T> gamePadBindings = new Dictionary<PlayerIndex, T>();
            gamePadBindings[PlayerIndex.One] = command;

            return gamePadBindings;
        }

        /// <summary>
        /// Draw a 2D texture with a scale factor
        /// </summary>
        /// <param name="spriteBatch">sprite batch to draw to</param>
        /// <param name="texture">texture to draw</param>
        /// <param name="position">screen position to draw to</param>
        /// <param name="scaleFactor">scale factor</param>
        /// <param name="color">color</param>
        protected void DrawWithScale(SpriteBatch spriteBatch, Texture2D texture, Vector2 position, Single scaleFactor, Color color)
        {
            spriteBatch.Draw(texture, position, null, color, 0, new Vector2(0.0f, 0.0f), scaleFactor, SpriteEffects.None, 0);
        }
    }
}
