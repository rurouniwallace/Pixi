﻿using Microsoft.Xna.Framework;
using Pixi.Graphics;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Scenes
{
    /// <summary>
    /// Abstraction for a scene dispatcher
    /// </summary>
    public interface ISceneQueue
    {
        /// <summary>
        /// Add a new scene to the queue
        /// </summary>
        public void AddSceneToQueue(Scene newScene);

        /// <summary>
        /// Update the currently running scene. If there are no scenes left, does nothing
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void UpdateCurrentScene(GameTime time);

        /// <summary>
        /// Draw the currently running scene. If there are no scenes left, draws a plain cornflower blue screen
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void DrawCurrentScene(GameTime time);
    }
}
