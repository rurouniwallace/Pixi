﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Pixi.Input.Commands;
using System.Collections.Generic;

namespace Pixi.Input
{
    public interface IListensForInput
    {
        Dictionary<PlayerIndex, Command> GamePadAButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadBackButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadBButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadBigButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadDownButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadLeftButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadLeftShoulderPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadLeftStickPressBinding { set; }
        Dictionary<PlayerIndex, ThumbStickCommand> GamePadLeftThumbStickBinding { set; }
        Dictionary<PlayerIndex, TriggerCommand> GamePadLeftTriggerBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadRightButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadRightShoulderPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadRightStickPressBinding { set; }
        Dictionary<PlayerIndex, ThumbStickCommand> GamePadRightThumbStickBinding { set; }
        Dictionary<PlayerIndex, TriggerCommand> GamePadRightTriggerBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadStartButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadUpButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadXButtonPressBinding { set; }
        Dictionary<PlayerIndex, Command> GamePadYButtonPressBinding { set; }
        Dictionary<Keys, Command> KeyPressBindings { set; }
        Dictionary<Keys, Command> KeyReleaseBindings { set; }
        MouseButtonCommand LeftMouseButtonPressBinding { set; }
        MouseButtonCommand LeftMouseButtonReleaseBinding { set; }
        MouseButtonCommand MiddleMouseButtonPressBinding { set; }
        MouseButtonCommand MiddleMouseButtonReleaseBinding { set; }
        MouseButtonCommand RightMouseButtonPressBinding { set; }
        MouseButtonCommand RightMouseButtonReleaseBinding { set; }
        MouseScrollCommand ScrollBinding { set; }

        /// <summary>
        /// Actions to take the moment a game pad is connected
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadConnectedBinding { set; }

        /// <summary>
        /// Actions to take the moment a game pad is disconnected
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadDisconnectedBinding { set; }

        /// <summary>
        /// Command bound to game pad DPad up button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadUpButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad DPad right button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad DPad down button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadDownButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad DPad left button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad button A click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadAButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad B button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad X button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadXButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad Y button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadYButtonReleaseBinding { set;  }

        /// <summary>
        /// Command bound to game pad back button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBackButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad big button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBigButtonReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftShoulderReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightShoulderReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftStickReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightStickReleaseBinding { set; }

        /// <summary>
        /// Command bound to game pad start button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadStartButtonReleaseBinding { set; }

        void ClearAllBindings();
        void ExecuteCommands(GameTime time);
    }
}