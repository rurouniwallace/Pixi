﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input.Commands
{
    /// <summary>
    /// Command triggered by a mouse scroll wheel
    /// </summary>
    /// <param name="currentScrollValue">scroll value at the current instant</param>
    /// <param name="previousScrollValue">scroll value at the previous frame</param>
    /// <param name="time">game timing state</param>
    public delegate void MouseScrollCommand(int currentScrollValue, int previousScrollValue, GameTime time);
}
