﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Pixi.Input.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Input
{
    public class InputListener : IListensForInput
    {
        #region Keyboard Bindings


        /// <summary>
        /// Commands bound to keyboard key presses
        /// </summary>
        public Dictionary<Keys, Command> KeyPressBindings { set; private get; }

        /// <summary>
        /// Commands bound to keyboard key releases
        /// </summary>
        public Dictionary<Keys, Command> KeyReleaseBindings { set; private get; }

        #endregion

        #region Mouse Bindings

        /// <summary>
        /// Command bound to left mouse button click
        /// </summary>
        public MouseButtonCommand LeftMouseButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to right mouse button click
        /// </summary>
        public MouseButtonCommand RightMouseButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to middle mouse button click
        /// </summary>
        public MouseButtonCommand MiddleMouseButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to left mouse button release
        /// </summary>
        public MouseButtonCommand LeftMouseButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to right mouse button release
        /// </summary>
        public MouseButtonCommand RightMouseButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to middle mouse button release
        /// </summary>
        public MouseButtonCommand MiddleMouseButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to mouse wheel scroll
        /// </summary>
        public MouseScrollCommand ScrollBinding { set; private get; }

        #endregion

        #region Game Pad Bindings

        /// <summary>
        /// Command bound to game pad DPad up button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadUpButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad right button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad down button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadDownButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad left button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad button A click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadAButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad B button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad X button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadXButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad Y button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadYButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad back button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBackButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad big button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBigButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftShoulderPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightShoulderPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftStickPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightStickPressBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad start button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadStartButtonPressBinding { set; private get; }

        /// <summary>
        /// Command bound to left game pad thumb stick
        /// </summary>
        public Dictionary<PlayerIndex, ThumbStickCommand> GamePadLeftThumbStickBinding { set; private get; }

        /// <summary>
        /// Command bound to right game pad thumb stick
        /// </summary>
        public Dictionary<PlayerIndex, ThumbStickCommand> GamePadRightThumbStickBinding { set; private get; }

        /// <summary>
        /// Command bound to the left game pad trigger
        /// </summary>
        public Dictionary<PlayerIndex, TriggerCommand> GamePadLeftTriggerBinding { set; private get; }

        /// <summary>
        /// Command bound to the right game pad trigger
        /// </summary>
        public Dictionary<PlayerIndex, TriggerCommand> GamePadRightTriggerBinding { set; private get; }

        /// <summary>
        /// Actions to take the moment a game pad is connected
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadConnectedBinding { set; private get; }

        /// <summary>
        /// Actions to take the moment a game pad is disconnected
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadDisconnectedBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad up button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadUpButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad right button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad down button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadDownButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad DPad left button press
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad button A click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadAButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad B button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad X button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadXButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad Y button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadYButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad back button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBackButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad big button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadBigButtonReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftShoulderReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightShoulderReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad left shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadLeftStickReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad right shoulder button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadRightStickReleaseBinding { set; private get; }

        /// <summary>
        /// Command bound to game pad start button click
        /// </summary>
        public Dictionary<PlayerIndex, Command> GamePadStartButtonReleaseBinding { set; private get; }

        #endregion

        #region Members

        /// <summary>
        /// Previous keyboard state
        /// </summary>
        private KeyboardState? _previousKeyboardState;

        /// <summary>
        /// Previous mouse state
        /// </summary>
        private MouseState? _previousMouseState;

        /// <summary>
        /// Previous gamepad state
        /// </summary>
        private Dictionary<PlayerIndex, GamePadState?> _previousGamePadState;

        #endregion

        #region Methods

        public InputListener()
        {
            ClearAllBindings();

            _previousKeyboardState = null;

            _previousMouseState = null;

            _previousGamePadState = new Dictionary<PlayerIndex, GamePadState?>();

            _previousGamePadState.Add(PlayerIndex.One, null);
            _previousGamePadState.Add(PlayerIndex.Two, null);
            _previousGamePadState.Add(PlayerIndex.Three, null);
            _previousGamePadState.Add(PlayerIndex.Four, null);
        }

        /// <summary>
        /// Execute all command bindings
        /// </summary>
        /// <param name="time">Game timing state</param>
        public void ExecuteCommands(GameTime time)
        {
            _executeKeyboardCommands(time);
            _executeMouseCommands(time);

            // up to four game pads could potentially be plugged in, so check and execute for each one
            PlayerIndex[] playerIndices = new PlayerIndex[] { PlayerIndex.One, PlayerIndex.Two, PlayerIndex.Three, PlayerIndex.Four };
            foreach (PlayerIndex playerIndex in playerIndices)
            {
                _executeGamePadCommands(playerIndex, time);
            }
        }

        /// <summary>
        /// Execute all keyboard-related commands
        /// </summary>
        /// <param name="time">Game timing state</param>
        private void _executeKeyboardCommands(GameTime time)
        {
            KeyboardState keyboardState = Keyboard.GetState();

            foreach (Keys key in KeyPressBindings.Keys)
            {
                if (keyboardState.IsKeyDown(key))
                {
                    KeyPressBindings[key](time);
                }
            }

            foreach (Keys key in KeyReleaseBindings.Keys)
            {
                if (_previousKeyboardState.HasValue && _previousKeyboardState.Value.IsKeyDown(key) && keyboardState.IsKeyUp(key))
                {
                    KeyReleaseBindings[key](time);
                }
            }

            _previousKeyboardState = keyboardState;
        }

        /// <summary>
        /// Execute all mouse-related commands
        /// </summary>
        /// <param name="time">Game timing state</param>
        private void _executeMouseCommands(GameTime time)
        {
            MouseState currentMouseState = Mouse.GetState();

            // if there's a left mouse click binding, check for a left mouse click and execute the command
            if (LeftMouseButtonPressBinding != null)
            {
                if (currentMouseState.LeftButton == ButtonState.Pressed)
                {
                    LeftMouseButtonPressBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // if there's a left mouse release binding, check for a left mouse release and execute the command
            if (LeftMouseButtonReleaseBinding != null)
            {
                if (_previousMouseState.HasValue && _previousMouseState.Value.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
                {
                    LeftMouseButtonReleaseBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // if there's a right mouse click binding, check for a right mouse click and execute the command
            if (RightMouseButtonPressBinding != null)
            {
                if (currentMouseState.RightButton == ButtonState.Pressed)
                {
                    RightMouseButtonPressBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // if there's a right mouse release binding, check for a right mouse release and execute the command
            if (RightMouseButtonReleaseBinding != null)
            {
                if (_previousMouseState.HasValue && _previousMouseState.Value.LeftButton == ButtonState.Pressed && currentMouseState.LeftButton == ButtonState.Released)
                {
                    RightMouseButtonReleaseBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // if there's a middle mouse click binding, check for a middle mouse click and execute the command
            if (MiddleMouseButtonPressBinding != null)
            {
                if (currentMouseState.MiddleButton == ButtonState.Pressed)
                {
                    MiddleMouseButtonPressBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // if there's a middle mouse release binding, check for a middle mouse release and execute the command
            if (MiddleMouseButtonReleaseBinding != null)
            {
                if (_previousMouseState.HasValue && _previousMouseState.Value.MiddleButton == ButtonState.Pressed && currentMouseState.MiddleButton == ButtonState.Released)
                {
                    MiddleMouseButtonReleaseBinding(currentMouseState.X, currentMouseState.Y, time);
                }
            }

            // TODO implement scroll wheel bindings here

            _previousMouseState = currentMouseState;
        }

        /// <summary>
        /// Execute game controller related bindings
        /// </summary>
        /// <param name="time">Game timing state</param>
        private void _executeGamePadCommands(PlayerIndex playerIndex, GameTime time)
        {
            GamePadState currentGamePadState = GamePad.GetState(playerIndex);

            if (_previousGamePadState[playerIndex].HasValue && !_previousGamePadState[playerIndex].Value.IsConnected && currentGamePadState.IsConnected)
            {
                Command connectedCommand = GamePadConnectedBinding.GetValueOrDefault(playerIndex);
                if (connectedCommand != null)
                {
                    connectedCommand(time);
                }
            }

            if (_previousGamePadState[playerIndex].HasValue && _previousGamePadState[playerIndex].Value.IsConnected && !currentGamePadState.IsConnected)
            {
                Command disconnectedCommand = GamePadDisconnectedBinding.GetValueOrDefault(playerIndex);
                if (disconnectedCommand != null)
                {
                    disconnectedCommand(time);
                }
            }

            if (currentGamePadState.IsConnected)
            {

                if (GamePad.GetCapabilities(playerIndex).HasDPadUpButton && currentGamePadState.DPad.Up == ButtonState.Pressed)
                {
                    Command upButtonCommand = GamePadUpButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (upButtonCommand != null)
                    {
                        upButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadUpButton && _previousGamePadState[playerIndex].HasValue && _previousGamePadState[playerIndex].Value.DPad.Up == ButtonState.Pressed && currentGamePadState.DPad.Up == ButtonState.Released)
                {
                    Command upButtonReleasedCommand = GamePadUpButtonReleaseBinding.GetValueOrDefault(playerIndex);
                    if (upButtonReleasedCommand != null)
                    {
                        upButtonReleasedCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadRightButton && currentGamePadState.DPad.Right == ButtonState.Pressed)
                {
                    Command rightButtonCommand = GamePadRightButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (rightButtonCommand != null)
                    {
                        rightButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadRightButton && _previousGamePadState[playerIndex].HasValue && _previousGamePadState[playerIndex].Value.DPad.Right == ButtonState.Pressed && currentGamePadState.DPad.Right == ButtonState.Released)
                {
                    Command rightButtonReleasedCommand = GamePadRightButtonReleaseBinding.GetValueOrDefault(playerIndex);
                    if (rightButtonReleasedCommand != null)
                    {
                        rightButtonReleasedCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadDownButton && currentGamePadState.DPad.Down == ButtonState.Pressed)
                {
                    Command downButtonCommand = GamePadDownButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (downButtonCommand != null)
                    {
                        downButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadDownButton && _previousGamePadState[playerIndex].HasValue && _previousGamePadState[playerIndex].Value.DPad.Down == ButtonState.Pressed && currentGamePadState.DPad.Down == ButtonState.Released)
                {
                    Command downButtonReleasedCommand = GamePadDownButtonReleaseBinding.GetValueOrDefault(playerIndex);
                    if (downButtonReleasedCommand != null)
                    {
                        downButtonReleasedCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadLeftButton && _previousGamePadState[playerIndex].HasValue && _previousGamePadState[playerIndex].Value.DPad.Left == ButtonState.Pressed && currentGamePadState.DPad.Left == ButtonState.Released)
                {
                    Command leftButtonReleasedCommand = GamePadLeftButtonReleaseBinding.GetValueOrDefault(playerIndex);
                    if (leftButtonReleasedCommand != null)
                    {
                        leftButtonReleasedCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasDPadLeftButton && currentGamePadState.DPad.Left == ButtonState.Pressed)
                {
                    Command leftButtonCommand = GamePadLeftButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (leftButtonCommand != null)
                    {
                        leftButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasAButton && currentGamePadState.Buttons.A == ButtonState.Pressed)
                {
                    Command aButtonCommand = GamePadAButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (aButtonCommand != null)
                    {
                        aButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasBButton && currentGamePadState.Buttons.B == ButtonState.Pressed)
                {
                    Command bButtonCommand = GamePadBButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (bButtonCommand != null)
                    {
                        bButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasXButton && currentGamePadState.Buttons.X == ButtonState.Pressed)
                {
                    Command xButtonCommand = GamePadXButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (xButtonCommand != null)
                    {
                        xButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasYButton && currentGamePadState.Buttons.Y == ButtonState.Pressed)
                {
                    Command yButtonCommand = GamePadYButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (yButtonCommand != null)
                    {
                        yButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasBackButton && currentGamePadState.Buttons.Back == ButtonState.Pressed)
                {
                    Command backButtonCommand = GamePadBackButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (backButtonCommand != null)
                    {
                        backButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasBigButton && currentGamePadState.Buttons.BigButton == ButtonState.Pressed)
                {
                    Command bigButtonCommand = GamePadBigButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (bigButtonCommand != null)
                    {
                        bigButtonCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasLeftShoulderButton && currentGamePadState.Buttons.LeftShoulder == ButtonState.Pressed)
                {
                    Command leftShoulderCommand = GamePadLeftShoulderPressBinding.GetValueOrDefault(playerIndex);
                    if (leftShoulderCommand != null)
                    {
                        leftShoulderCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasRightShoulderButton && currentGamePadState.Buttons.RightShoulder == ButtonState.Pressed)
                {
                    Command rightShoulderCommand = GamePadRightShoulderPressBinding.GetValueOrDefault(playerIndex);
                    if (rightShoulderCommand != null)
                    {
                        rightShoulderCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasLeftStickButton && currentGamePadState.Buttons.LeftStick == ButtonState.Pressed)
                {
                    Command leftStickPressCommand = GamePadLeftStickPressBinding.GetValueOrDefault(playerIndex);
                    if (leftStickPressCommand != null)
                    {
                        leftStickPressCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasRightStickButton && currentGamePadState.Buttons.RightStick == ButtonState.Pressed)
                {
                    Command rightStickPressCommand = GamePadRightStickPressBinding.GetValueOrDefault(playerIndex);
                    if (rightStickPressCommand != null)
                    {
                        rightStickPressCommand(time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasStartButton && currentGamePadState.Buttons.Start == ButtonState.Pressed)
                {
                    Command startButtonCommand = GamePadStartButtonPressBinding.GetValueOrDefault(playerIndex);
                    if (startButtonCommand != null)
                    {
                        startButtonCommand(time);
                    }
                }

                // I'm not aware of any scenario where only one axis of the thumbstick is available but not the other,
                // but XNA counts them as two separate capabilities so we're checking both
                if (GamePad.GetCapabilities(playerIndex).HasLeftXThumbStick && GamePad.GetCapabilities(playerIndex).HasLeftYThumbStick)
                {
                    ThumbStickCommand leftThumbStickCommand = GamePadLeftThumbStickBinding.GetValueOrDefault(playerIndex);
                    if (leftThumbStickCommand != null)
                    {
                        Vector2? previousPosition = null;
                        if (_previousGamePadState[playerIndex].HasValue)
                        {
                            previousPosition = _previousGamePadState[playerIndex].Value.ThumbSticks.Left;
                        }
                        leftThumbStickCommand(previousPosition, currentGamePadState.ThumbSticks.Left, time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasRightXThumbStick && GamePad.GetCapabilities(playerIndex).HasRightYThumbStick)
                {
                    ThumbStickCommand rightThumbStickCommand = GamePadRightThumbStickBinding.GetValueOrDefault(playerIndex);
                    if (rightThumbStickCommand != null)
                    {
                        Vector2? previousPosition = null;
                        if (_previousGamePadState[playerIndex].HasValue)
                        {
                            previousPosition = _previousGamePadState[playerIndex].Value.ThumbSticks.Right;
                        }
                        rightThumbStickCommand(previousPosition, currentGamePadState.ThumbSticks.Right, time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasLeftTrigger)
                {
                    TriggerCommand leftTriggerCommand = GamePadLeftTriggerBinding.GetValueOrDefault(playerIndex);
                    if (leftTriggerCommand != null)
                    {
                        leftTriggerCommand(currentGamePadState.Triggers.Left, time);
                    }
                }

                if (GamePad.GetCapabilities(playerIndex).HasRightTrigger)
                {
                    TriggerCommand rightTriggerCommand = GamePadRightTriggerBinding.GetValueOrDefault(playerIndex);
                    if (rightTriggerCommand != null)
                    {
                        rightTriggerCommand(currentGamePadState.Triggers.Right, time);
                    }
                }

                // TODO implement button release bindings
            }
            _previousGamePadState[playerIndex] = currentGamePadState;

        }



        /// <summary>
        /// Clear all currently set input bindings
        /// </summary>
        public void ClearAllBindings()
        {
            KeyPressBindings = new Dictionary<Keys, Command>();
            KeyReleaseBindings = new Dictionary<Keys, Command>();

            LeftMouseButtonPressBinding = null;
            RightMouseButtonPressBinding = null;
            MiddleMouseButtonPressBinding = null;

            LeftMouseButtonReleaseBinding = null;
            RightMouseButtonReleaseBinding = null;
            MiddleMouseButtonReleaseBinding = null;

            GamePadUpButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadRightButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadDownButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadLeftButtonPressBinding = new Dictionary<PlayerIndex, Command>();

            GamePadAButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadXButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadYButtonPressBinding = new Dictionary<PlayerIndex, Command>();

            GamePadStartButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBackButtonPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBigButtonPressBinding = new Dictionary<PlayerIndex, Command>();

            GamePadLeftShoulderPressBinding = new Dictionary<PlayerIndex, Command>();
            GamePadRightShoulderPressBinding = new Dictionary<PlayerIndex, Command>();

            GamePadUpButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadRightButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadDownButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadLeftButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();

            GamePadAButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadXButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadYButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();

            GamePadStartButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBackButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadBigButtonReleaseBinding = new Dictionary<PlayerIndex, Command>();

            GamePadLeftShoulderReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadRightShoulderReleaseBinding = new Dictionary<PlayerIndex, Command>();

            GamePadLeftStickReleaseBinding = new Dictionary<PlayerIndex, Command>();
            GamePadRightStickReleaseBinding = new Dictionary<PlayerIndex, Command>();

            GamePadLeftThumbStickBinding = new Dictionary<PlayerIndex, ThumbStickCommand>();
            GamePadRightThumbStickBinding = new Dictionary<PlayerIndex, ThumbStickCommand>();

            GamePadLeftTriggerBinding = new Dictionary<PlayerIndex, TriggerCommand>();
            GamePadRightTriggerBinding = new Dictionary<PlayerIndex, TriggerCommand>();

            GamePadConnectedBinding = new Dictionary<PlayerIndex, Command>();
            GamePadDisconnectedBinding = new Dictionary<PlayerIndex, Command>();

            ScrollBinding = null;
        }

        #endregion
    }
}
