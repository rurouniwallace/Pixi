﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pixi.Util
{
    /// <summary>
    /// Utility to exit the game
    /// </summary>
    class GameExiter : IExitsGame
    {
        /// <summary>
        /// The game
        /// </summary>
        private Game _game;

        /// <summary>
        /// Construct a new instance
        /// </summary>
        /// <param name="game">Game to exit</param>
        public GameExiter(Game game)
        {
            _game = game;
        }

        /// <summary>
        /// Exit the game
        /// </summary>
        public void ExitGame()
        {
            _game.Exit();
        }
    }
}
